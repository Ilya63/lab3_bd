#### RideCleansingExercise
![Alt text](Lab3_images/RideCleansingExercise.png?raw=true "RideCleansingExercise")
<br>
#### RidesAndFaresExercise
![Alt text](Lab3_images/RidesAndFaresExercise.png?raw=true "RidesAndFaresExercise")
<br>
#### HourlyTipsExercise
![Alt text](Lab3_images/HourlyTipsExercise.png?raw=true "HourlyTipsExerxise")
<br>
#### ExpiringStateExercise
![Alt text](Lab3_images/ExpiringStateExercise.png?raw=true "ExpiringStateExercise")
<br><br>
### Тесты

#### RideCleansing
![Alt text](Lab3_images_test/RideCleansingScalaTest.png?raw=true)
<br>
#### RidesAndFares
![Alt text](Lab3_images_test/RidesAndFaresScalaTest.png?raw=true)
<br>
#### HourlyTips
![Alt text](Lab3_images_test/HourlyTipsScalaTest.png?raw=true)
<br>
#### ExpiringState
![Alt text](Lab3_images_test/ExpiringStateScalaTest.png?raw=true)
